# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
  if $('.pagination').length
    $(window).scroll ->
      url = $('.pagination .next_page').attr('href')
      console.log $(window).scrollTop
      if url && $(window).scrollTop() > $(document).height() - $(window).height() - 50
        $('.pagination').text ('Loading...')
        $.getScript(url)
        console.log "Ok!"
